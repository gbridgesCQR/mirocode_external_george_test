#!/bin/bash

# this repository is a template for an external repository
# that a mirocode user might supply. in this script, we do
# what mirocode will do, that is add the various paths to
# gazebo search lists, and launch gazebo with the example
# world file included.

# augment search paths
GAZEBO_RESOURCE_PATH=$GAZEBO_RESOURCE_PATH:$PWD
GAZEBO_PLUGIN_PATH=$GAZEBO_PLUGIN_PATH:$PWD/plugins
GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:$PWD/models

# we don't need to add 'worlds', we just point gazebo at
# it when launching the world.
gazebo --verbose worlds/virtual_ralt_standalone.world

